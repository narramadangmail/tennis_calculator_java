# Tennis Calculator

## Challenge

The tennis calculator takes a set of scores as inputs and produces useful statistics based on those scores.

This calculator will use a simplified version of scoring where whoever gets to 6 games first wins the set.

Refer to the [Challenge Document](Challenge.md) for complete understanding on the expected input, output and rules to be applied for the calculator 

## Considerations

Below are few considerations for the program on expected tournament data & query requests fed to the program:

* The first row is a match id, the second row shows who is playing against whom.
* After that are a series of points, where `0` is a point for the `first person` listed, `1` is for `last person`.
* Below are two queries supported by the program
    * Query scores for a particular match - `Score Match <id>`
    * Summary of games won vs lost for a particular player over the tournament - `Games Player <Player Name>`

## Sample Input & Expected Output

Below is the sample request and expected response for the provided `full_tournament.txt` file for this challenge

```
$ java -jar target\tenniscalculator-1.0-SNAPSHOT-jar-with-dependencies.jar
D:\Work\samples\tennis_calculator\full_tournament.txt
Score Match 01
Score Match 02
Games Player Person A
EOF

Person A defeated Person B
2 sets to 0

Person C defeated Person A
2 sets to 1

23 17
```

## Technologies

Tennis Calculator is implemented in `Java 11` as `Maven` Project. Following are the libraries which are managed by Maven:

* Apache Commons - Lang, Collections & IO
* JUnit 5
* JaCoCo for Code Coverage

## Configuring and Building Tennis Calculator program

Follow the below steps to build and execute Tennis Calculator on your machine.

* Ensure you have `Java 11` installed and configured before proceeding further. If not available, follow the below steps to set up Java 11
    * Download [Zulu OpenJDK 11 Win x64 MSI](https://cdn.azul.com/zulu/bin/zulu11.45.27-ca-jdk11.0.10-win_x64.msi) or choose [appropriate](https://www.azul.com/downloads/zulu-community/?version=java-11-lts&package=jdk) based on your machine.
    * Proceed to execute the installer by choosing the appropriate location or stick to default.
    * Configure `PATH` & `JAVA_HOME` system environment variable to point to the installation location. Follow [Instructions](https://javatutorial.net/set-java-home-windows-10) for Windows 10. 
    * Run `java -version` in your command prompt to confirm Java is configured and working. 

* Clone the project from [Bitbucket Repo](https://bitbucket.org/narramadangmail/tennis_calculator_java/src/master/) or extract the provided project archive to your respective location.
```
$ git clone https://narramadangmail@bitbucket.org/narramadangmail/tennis_calculator_java.git
```

* Run below `maven` commands in command prompt from the root of the application. This should compile, execute tests and build `Executable JAR`.
    * Initial build will take couple of minutes to download packages and dependencies required to build the application. Subsequent builds will be faster. 
```
$ .\mvnw.cmd clean package
[INFO] Scanning for projects...
[INFO]
[INFO] ---------------< org.tennis.calculator:tenniscalculator >---------------
[INFO] Building tenniscalculator 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
....
....
....
[INFO] Tests run: 15, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.093 s - in org.tennis.calculator.ScoreCalculatorTest
[INFO] Running org.tennis.calculator.TournamentFileValidatorTest
Incomplete game data provided for match - 01
Incomplete game data provided for match - 02
[INFO] Tests run: 8, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.096 s - in org.tennis.calculator.TournamentFileValidatorTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 29, Failures: 0, Errors: 0, Skipped: 0
....
....
....
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  02:23 min
[INFO] Finished at: 2021-02-28T14:54:23+11:00
[INFO] ------------------------------------------------------------------------
```

## Executing Tennis Calculator

Upon building the program with maven, A `Executable Jar` will be created under `target` folder. 

* Run the below command in command prompt from the root of the application to start the application
```
$ java -jar target\tenniscalculator-1.0-SNAPSHOT-jar-with-dependencies.jar
```

* Copy & paste the below sample input to command prompt. 
* This should pick the tournament data file available at root of the application.  
```
full_tournament.txt
Score Match 01
Score Match 02
Games Player Person A
EOF
```

* If you wish to use a different tournament file
    * Start the application 
    * Replace `full_tournament.txt` with complete path to your tournament file in above sample input
    * Copy & paste to command prompt  

* Copy & paste the below input to test the application with larger tournament data set with `5000 matches`.

```
java -jar target\tenniscalculator-1.0-SNAPSHOT-jar-with-dependencies.jar
full_tournament_couple_thousand_matches.txt
Score Match 1013
Score Match 2025
Score Match 5557
Score Match 9999
Games Player Person A
Games Player Person B
Games Player Person C
Games Player Person D
EOF
```

## Solution

Catering to the way tournament data is provided in the file with no proper separation or delimiters, I felt it would not be ideal to read the data file line by line and process the data. Rather, I planned to `read the whole file` to a `String` object on start my data processing.

Below is the high level pseudo-code of my solution
* Read the complete tournament data file to string object
* Split the string by line break & new line character - `\r\n`. This would return me the data as list of String objects.
* Filter out empty strings and prepare the final `List<String>` object to start the data processing
* I created modal objects to hold the information inline to the requirement
```
Tournament (contains) List<Match>
            |
            v
Match (contains) Player[2]
            |
            v
Player (holds) Game Points & Games Won
```
* Loop through the list of strings and process them based on the pattern they match
    * Create new Match object and add it to tournament.
    * Initialize player objects and assign to the current match being processed.
    * Increment player game point based on who won the point. Post incrementing, validate if the player won the game. If yes, then increment the games won and reset the game points for both the players.

* Once the Tournament modal is finished, it will be used to process the queries provided as input.

* Validations are added wherever applicable to halt the process execution with proper feedback message, so the user can fix the data file and rerun the execution.  

## Test Driven Development

Tennis Calculation application is implemented by following incremental `TDD` approach.

After analyzing the challenge requirements, I created couple of JUnit test cases with all possible negative scenarios that could fail processing the application.

Some of the negative tests I started with are as below:

* Tournament file not provided, not found and file is empty.
* Query request not provided, not inline to the expected format.
* Duplicate match names in the tournament file
* Duplicate player names tagged to the match
* Match containing more than two players
* Invalid content other than what should be part of the tournament data file

With the above negative tests configured, I started with the implementation ensuring to pass these tests incrementally.

As the program evolved, more number of tests where added to consider few other tests which I didn't capture initially.

With the base application working with all possible tests, I refactored the code by rearranging the bits and pieces to their respective classes.

To test processing for different tournament data set, created data files under `src\main\test\resources\tournament_samples`.

Finally, created `@ParameterizedTest` wherever applicable to pass different data set and verify for expected outcome. This will ensure to test the same piece of logic with multiple data sets.

Below are the unit test results captured upon execute them from `IntelliJ IDEA`

![Unit Tests Result](unit_tests.png)

## Code Coverage Results

`JaCoCo` is configured with maven to capture the code covered as part of the unit tests that are executed when packaging the application.

To generate the report, run `.\mvnw.cmd clean verify` from the root of the project. This will generate the coverage results in `target\site\jacoco` folder. 

Open index.html to view the results. It should see something as below:

![Code Coverage Result](code_coverage.png)

## Further Improvements

To my understanding, The implemented application meets most of the challenge requirements. There could be few more additional tests that can be added for scenarios I missed.

As any application, there can be lot more improvements be made to extend it to the next level. Below are few which I can think of: 

* Provide prompts to user to enter path to tournament data file or search query.
* Ability for user to provide queries one by one instead of builk input.
* Ability to capture all the query requests and export them to file for future reference.
* Move hardcoded search patterns or exception messages into resources file.
