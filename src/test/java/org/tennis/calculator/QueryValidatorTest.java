package org.tennis.calculator;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.tennis.calculator.model.Match;
import org.tennis.calculator.model.Player;
import org.tennis.calculator.model.Tournament;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.stream.Stream;

import static org.tennis.calculator.Constants.QueryRequest;

public class QueryValidatorTest {

    private static QueryProcessor queryProcessor;
    private static Tournament tournamentData;

    @BeforeAll
    public static void setUp() {
        queryProcessor = new QueryProcessor();
        tournamentData = prepareTournamentData();
    }

    @ParameterizedTest
    @DisplayName("Validate query provided to the program is of valid format")
    @MethodSource("queryRequestData")
    public void testValidQueryFormat(List<String> queryRequests, boolean validRequest) {

        System.out.println(String.format("%s - %s ", queryRequests, validRequest));
        try {
            queryProcessor.processRequestQueries(tournamentData, queryRequests);

            // If not valid request, then exception should be thrown
            if(!validRequest) fail();
        }
        catch (CalculatorException e) {

            // If valid request, then exception should not be thrown
            if(validRequest) fail();
        }
    }

    static Stream<Arguments> queryRequestData() {
        return Stream.of(
            Arguments.of(List.of(
                String.format("%s Person A", QueryRequest.GAMES_PLAYER.prefix),
                String.format("%s 01", QueryRequest.SCORE_MATCH.prefix)),
                true),
            Arguments.of(List.of(
                String.format("%sPerson A", QueryRequest.GAMES_PLAYER.prefix),
                String.format("%s01", QueryRequest.SCORE_MATCH.prefix)),
                true),
            Arguments.of(List.of(
                String.format("%s Person A", QueryRequest.GAMES_PLAYER.prefix+"INVALID"),
                String.format("%s 01", QueryRequest.SCORE_MATCH.prefix+"INVALID")),
                false),
            Arguments.of(List.of(
                String.format("%s Person A", QueryRequest.GAMES_PLAYER.prefix),
                String.format("%s 01", "INVALID"+QueryRequest.SCORE_MATCH.prefix)),
                false),
            Arguments.of(List.of(
                String.format("%s Person A", "INVALID"+QueryRequest.GAMES_PLAYER.prefix),
                String.format("%s 01", QueryRequest.SCORE_MATCH.prefix)),
                false),
            Arguments.of(List.of(),false)
        );
    }

    private static Tournament prepareTournamentData() {
        Match match = new Match();
        match.setName("01");

        Player playerA = new Player("Person A");
        playerA.markSetsWon();
        playerA.markSetsWon();

        Player playerB = new Player("Person B");

        match.getPlayers()[0] = playerA;
        match.getPlayers()[1] = playerB;

        Tournament tournament = new Tournament();
        tournament.getMatches().add(match);

        return tournament;
    }
}
