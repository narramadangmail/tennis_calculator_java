package org.tennis.calculator;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.tennis.calculator.model.Player;
import org.tennis.calculator.util.NullableConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ScoreCalculatorTest {

    @ParameterizedTest
    @DisplayName("Verify if there are valid points for player to win the game")
    @CsvSource({
        "0,0,null",
        "4,0,player0",
        "0,4,player1",
        "3,3,null",
        "4,4,null",
        "5,4,null",
        "4,5,null",
        "6,4,player0",
        "4,6,player1",
        "7,6,null",
        "9,7,player0",
        "7,9,player1"})
    public void testHasValidPointsToWinGame(int playerAPoints, int playerBPoints, @ConvertWith(NullableConverter.class) String player) {

        System.out.println(String.format("%s - %s - %s", playerAPoints, playerBPoints, player));

        assertEquals(ScoreCalculator.calculateGameWin(player, playerAPoints, playerBPoints), player);
    }

    @ParameterizedTest
    @DisplayName("Verify if there are valid sets for player to win the match")
    @MethodSource("testDataForValidSetsToWinMatch")
    public void testHasValidSetsToWinMatch(Player playerA, Player playerB, @ConvertWith(NullableConverter.class) String player) {
        System.out.println(String.format("%s - %s - %s", playerA.getSetsWon(), playerB.getSetsWon(), player));

        Player playerWon = ScoreCalculator.calculateMatchWinner(playerA, playerB);

        assertEquals(playerWon != null ? playerWon.getName() : null, player);
    }

    static Stream<Arguments> testDataForValidSetsToWinMatch() {

        List<Arguments> arguments = new ArrayList<>();

        // Set player0 with 2 sets won
        final Player player0 = new Player("Player A");
        IntStream.of(0,2).forEach(i -> {
            player0.markSetsWon();
        });
        Player player1 = new Player("Player B");
        arguments.add(Arguments.of(player0,player1, player0.getName()));

        // Set player1 with 2 sets won
        Player player00 = new Player("Player A");
        final Player player01 = new Player("Player B");
        IntStream.of(0, 2).forEach(i -> {
            player01.markSetsWon();
        });
        arguments.add(Arguments.of(player00,player01, player01.getName()));

        // Set players won only single set
        Player player10 = new Player("Player A");
        Player player11 = new Player("Player B");
        arguments.add(Arguments.of(player10,player11, null));

        return arguments.stream();
    }
}