package org.tennis.calculator;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.tennis.calculator.model.Tournament;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TournamentFileValidatorTest {

    private static TournamentDataProcessor tournamentDataProcessor;
    private static final String TOURNAMENT_DATA_FILEPATH = "src/test/resources/tournament_samples/";

    @BeforeAll
    private static void setUp() {
        tournamentDataProcessor = new TournamentDataProcessor();
    }

    @Test
    public void testTournamentFileNotFound() {
        CalculatorException exception = assertThrows(CalculatorException.class, () -> {
            tournamentDataProcessor.parseTournamentFile("INVALID_PATH");
        });

        assertEquals(CalculatorException.ErrorCode.TOURNAMENT_FILE_NOT_FOUND, exception.getError());
    }

    @Test
    public void testFileIsEmpty() {

        File file = new File(TOURNAMENT_DATA_FILEPATH+"tournament_nodata.txt");
        final String absolutePath = file.getAbsolutePath();

        CalculatorException exception = assertThrows(CalculatorException.class, () -> {
            tournamentDataProcessor.parseTournamentFile(absolutePath);
        });

        assertEquals(CalculatorException.ErrorCode.TOURNAMENT_FILE_EMPTY, exception.getError());
    }

    @Test
    public void testTournamentDataCorrectness() {

        CalculatorException exception = assertThrows(CalculatorException.class, () -> {
            File file = new File(TOURNAMENT_DATA_FILEPATH+"tournament_invalid_data.txt");
            final String absolutePath = file.getAbsolutePath();

            List<String> tournamentData = tournamentDataProcessor.parseTournamentFile(absolutePath);
            Tournament tournament = tournamentDataProcessor.processTournamentData(tournamentData);

            TournamentDataValidator.validateTournamentData(tournament);

        });

        assertEquals(CalculatorException.ErrorCode.INVALID_CONTENT_IN_TOURNAMENT_FILE, exception.getError());
    }

    @Test
    public void testTournamentMatchNotHavingTwoPlayers() {
        CalculatorException exception = assertThrows(CalculatorException.class, () -> {
            File file = new File(TOURNAMENT_DATA_FILEPATH+"tournament_more_than_two_players.txt");
            final String absolutePath = file.getAbsolutePath();

            List<String> tournamentData = tournamentDataProcessor.parseTournamentFile(absolutePath);
            Tournament tournament = tournamentDataProcessor.processTournamentData(tournamentData);

            TournamentDataValidator.validateTournamentData(tournament);

        });

        assertEquals(CalculatorException.ErrorCode.MATCH_CONTAINS_MORE_THAN_TWO_PLAYERS, exception.getError());
    }

    @Test
    public void testTournamentHavingDuplicateMatches() {

        CalculatorException exception = assertThrows(CalculatorException.class, () -> {
            File file = new File(TOURNAMENT_DATA_FILEPATH+"tournament_duplicate_matches.txt");
            final String absolutePath = file.getAbsolutePath();

            List<String> tournamentData = tournamentDataProcessor.parseTournamentFile(absolutePath);
            Tournament tournament = tournamentDataProcessor.processTournamentData(tournamentData);

            TournamentDataValidator.validateTournamentData(tournament);

        });

        assertEquals(CalculatorException.ErrorCode.DUPLICATE_MATCH_NAMES, exception.getError());
    }

    @Test
    public void testTournamentHavingDuplicatePlayers() {
        CalculatorException exception = assertThrows(CalculatorException.class, () -> {
            File file = new File(TOURNAMENT_DATA_FILEPATH+"tournament_duplicate_players.txt");
            final String absolutePath = file.getAbsolutePath();

            List<String> tournamentData = tournamentDataProcessor.parseTournamentFile(absolutePath);
            Tournament tournament = tournamentDataProcessor.processTournamentData(tournamentData);

            TournamentDataValidator.validateTournamentData(tournament);

        });

        assertEquals(CalculatorException.ErrorCode.DUPLICATE_PLAYERS_FOR_MATCH, exception.getError());
    }

    @ParameterizedTest
    @CsvSource({
        "tournament_incomplete_single_match_data.txt",
        "tournament_incomplete_match_data.txt"
    })
    public void testTournamentHavingIncompleteMatchData(final String tournamentFile) {
        CalculatorException exception = assertThrows(CalculatorException.class, () -> {
            File file = new File(TOURNAMENT_DATA_FILEPATH+tournamentFile);
            final String absolutePath = file.getAbsolutePath();

            List<String> tournamentData = tournamentDataProcessor.parseTournamentFile(absolutePath);
            Tournament tournament = tournamentDataProcessor.processTournamentData(tournamentData);

            TournamentDataValidator.validateTournamentData(tournament);

        });

        System.out.println(exception.getMessage());
        assertEquals(CalculatorException.ErrorCode.GAME_DATA_NOT_COMPLETE, exception.getError(), exception.getMessage());
    }
}
