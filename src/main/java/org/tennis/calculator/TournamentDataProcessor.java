package org.tennis.calculator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.tennis.calculator.model.Match;
import org.tennis.calculator.model.Player;
import org.tennis.calculator.model.Tournament;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.tennis.calculator.Constants.*;
import static org.tennis.calculator.CalculatorException.*;

public class TournamentDataProcessor {

    /**
     * Method to parse the file and prepare file content into stream of strings
     * @param fileName
     * @return tournamentData
     * @throws CalculatorException
     */
    public List<String> parseTournamentFile(String fileName) throws CalculatorException {

        List<String> tournamentData = null;
        try {
            // Read content of tournament data to string
            String content = FileUtils.readFileToString(Paths.get(fileName).toFile(), Charset.defaultCharset());

            // Split tournament data into stream of strings for further processing
            tournamentData
                = Arrays.stream(content.split("\r\n"))
                    .filter(Predicate.not(String::isEmpty)).collect(Collectors.toList());

            // Throw error if content is empty
            if(CollectionUtils.isEmpty(tournamentData)) {
                throw new CalculatorException(ErrorCode.TOURNAMENT_FILE_EMPTY, String.format(ErrorCode.TOURNAMENT_FILE_EMPTY.description, fileName));
            }
        }
        catch (IOException e) {
            throw new CalculatorException(ErrorCode.TOURNAMENT_FILE_NOT_FOUND, String.format(ErrorCode.TOURNAMENT_FILE_NOT_FOUND.description, fileName));
        }
        return tournamentData;
    }

    /**
     * Process tournament data with matches and player stats
     * @param tournamentData
     * @return tournament
     * @throws CalculatorException
     */
    public Tournament processTournamentData(List<String> tournamentData) throws CalculatorException {
        if(CollectionUtils.isNotEmpty(tournamentData)) {
            final Tournament tournament = new Tournament();
            try {

                // Stream through tournament data and create match specific records
                final AtomicReference<Match> currentMatch = new AtomicReference<>();
                tournamentData.stream().forEach(data -> {
                    try {
                        if (data.contains(TournamentDataMatches.MATCH_DATA.matches)) {

                            // Identify if current game data is complete before processing next match
                            if(currentMatch.get() != null) verifyIfGameDataIncomplete(currentMatch.get());

                            currentMatch.set(new Match());
                            currentMatch.get().setName(data.replace(TournamentDataMatches.MATCH_DATA.matches, "").trim());

                            tournament.getMatches().add(currentMatch.get());
                        }
                        else if (data.contains(TournamentDataMatches.PLAYERS_SEPARATOR.matches)) {
                            // Split players line by vs
                            String[] players = Arrays.stream(data.split(TournamentDataMatches.PLAYERS_SEPARATOR.matches)).map(String::trim).toArray(String[]::new);

                            // Validate if there are only two players for the match
                            if (players.length != 2) {
                                throw new CalculatorException(
                                    ErrorCode.MATCH_CONTAINS_MORE_THAN_TWO_PLAYERS,
                                    String.format(ErrorCode.MATCH_CONTAINS_MORE_THAN_TWO_PLAYERS.description, data));
                            }

                            // Set players to match
                            Match match = currentMatch.get();
                            match.setPlayers(players[0], players[1]);
                        }
                        // Validate for rest of data should be either 0 or 1
                        else if (data.matches("^[01]+$")) {

                            processGameDate(currentMatch.get(), Integer.parseInt(data));
                        }
                        else {
                            throw new CalculatorException(
                                ErrorCode.INVALID_CONTENT_IN_TOURNAMENT_FILE,
                                String.format(ErrorCode.INVALID_CONTENT_IN_TOURNAMENT_FILE.description, data));
                        }

                    } catch (CalculatorException e) {
                        throw new WrappedException(e);
                    }
                });

                // Identify if current game data is complete before finishing the data processing
                if(currentMatch.get() != null) verifyIfGameDataIncomplete(currentMatch.get());
            }
            catch (WrappedException e) {
                if(e.getCause() instanceof CalculatorException) {
                    throw (CalculatorException) e.getCause();
                }
            }
            return tournament;
        }
        return null;
    }

    private Match processGameDate(Match match, int playerIndex) throws CalculatorException {

        // Mark Game point by who player
        match.getPlayers()[playerIndex].markGamePoint();

        // Identify if the game is complete by identifying the winner of the game
        String playerWinningGame
            = ScoreCalculator.calculateGameWin(
                match.getPlayers()[playerIndex].getName(),
                match.getPlayers()[0].getGamePoint(),
                match.getPlayers()[1].getGamePoint());

        if(StringUtils.isNoneBlank(playerWinningGame)) {

            // Mark the game as won for the player winning the game
            Player winningPlayer = Arrays.stream(match.getPlayers()).filter(player -> player.getName().equals(playerWinningGame)).findFirst().get();
            winningPlayer.markGameWon();

            // Reset game points
            match.getPlayers()[0].resetGamePoints();
            match.getPlayers()[1].resetGamePoints();
        }
        return match;
    }

    // Verify if game data is not complete for the match
    private void verifyIfGameDataIncomplete(Match match) throws CalculatorException {
        try {
            Arrays.stream(match.getPlayers()).forEach(player -> {
                try {
                    if (player.getGamePoint() != 0)
                        throw new CalculatorException(
                            ErrorCode.GAME_DATA_NOT_COMPLETE,
                            String.format(ErrorCode.GAME_DATA_NOT_COMPLETE.description, match.getName()));
                } catch (CalculatorException e) {
                    throw new WrappedException(e);
                }
            });
        }
        catch (WrappedException e) {
            if(e.getCause() instanceof CalculatorException) throw (CalculatorException) e.getCause();
        }
    }
}
