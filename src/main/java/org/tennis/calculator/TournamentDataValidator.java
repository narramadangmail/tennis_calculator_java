package org.tennis.calculator;

import org.apache.commons.collections4.CollectionUtils;
import org.tennis.calculator.model.Match;
import org.tennis.calculator.model.Player;
import org.tennis.calculator.model.Tournament;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TournamentDataValidator {

    /**
     * Validate tournament data for any data quality issues
     * @param tournament
     * @throws CalculatorException
     */
    public static void validateTournamentData(Tournament tournament) throws CalculatorException {

        verifyForDuplicateMatches(tournament);

        verifyForDuplicatePlayers(tournament);
    }

    private static void verifyForDuplicateMatches(Tournament tournament) throws CalculatorException {

        List<Match> matches
            = tournament.getMatches().stream().collect(Collectors.groupingBy(match -> match.getName(),Collectors.toList()))
                .values().stream().filter(i->i.size()>1).flatMap(j->j.stream()).collect(Collectors.toList());

        if(matches.size() > 0)
            throw new CalculatorException(CalculatorException.ErrorCode.DUPLICATE_MATCH_NAMES, CalculatorException.ErrorCode.DUPLICATE_MATCH_NAMES.description);
    }

    private static void verifyForDuplicatePlayers(Tournament tournament) throws CalculatorException {
        try {

            tournament.getMatches().stream().forEach(match -> {
                try {

                    List<Player> players
                            = Arrays.stream(match.getPlayers()).collect(Collectors.groupingBy(player -> player.getName(),Collectors.toList()))
                            .values().stream().filter(i->i.size()>1).flatMap(j->j.stream()).collect(Collectors.toList());

                    if (CollectionUtils.isNotEmpty(players) && players.size() > 0)
                        throw new CalculatorException(
                            CalculatorException.ErrorCode.DUPLICATE_PLAYERS_FOR_MATCH,
                            String.format(CalculatorException.ErrorCode.DUPLICATE_PLAYERS_FOR_MATCH.description, match.getName()));
                }
                catch (CalculatorException e) {
                    throw new WrappedException(e);
                }
            });

        }
        catch (WrappedException e) {
            if(e.getCause() instanceof CalculatorException) throw (CalculatorException) e.getCause();
        }
    }
}
