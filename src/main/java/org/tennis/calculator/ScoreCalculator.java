package org.tennis.calculator;

import org.tennis.calculator.model.Player;

public class ScoreCalculator {

    /**
     * Method to identify the player who won the game
     * @param player
     * @param player0Score
     * @param player1Score
     * @return playerWon
     */
    public static String calculateGameWin(String player, int player0Score, int player1Score) {
        return
            (player0Score == 4 && player1Score < 4) ||  (player0Score > 4 && player0Score - player1Score >= 2)
                ? player
                : (player1Score == 4 && player0Score < 4) || (player1Score > 4 && player1Score - player0Score >=2)
                    ? player : null;
    }

    /**
     * Method to identify player who won the match
     * @param player0
     * @param player1
     * @return playerWon
     */
    public static Player calculateMatchWinner(Player player0, Player player1) {
        return
            (player0.getSetsWon() == 2)
                ? player0
                : (player1.getSetsWon() == 2)
                    ? player1 : null;
    }

}
