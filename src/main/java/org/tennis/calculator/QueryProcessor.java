package org.tennis.calculator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.tennis.calculator.model.Match;
import org.tennis.calculator.model.Player;
import org.tennis.calculator.model.Tournament;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class QueryProcessor {

    public List<String> processRequestQueries(final Tournament tournament, List<String> queries) throws CalculatorException {
        List<String> queryResponses = new ArrayList<>();
        try {
            if (CollectionUtils.isNotEmpty(queries)) {
                queries.stream().forEach(query -> {
                    try {
                        queryResponses.add(processQuery(tournament, query));
                    } catch (CalculatorException e) {
                        throw new WrappedException(e);
                    }
                });
            }
            else {
                throw new CalculatorException(CalculatorException.ErrorCode.NO_QUERY_TO_PROCESS, CalculatorException.ErrorCode.NO_QUERY_TO_PROCESS.description);
            }
        }
        catch (WrappedException e) {
            if(e.getCause() instanceof CalculatorException) throw (CalculatorException) e.getCause();
        }
        return queryResponses;
    }

    private String processQuery(Tournament tournament, String query) throws CalculatorException {
        String response = null;

        if(query.startsWith(Constants.QueryRequest.SCORE_MATCH.prefix)) {
            response = processScoreMatchQuery(tournament, query);
        }
        else if(query.startsWith(Constants.QueryRequest.GAMES_PLAYER.prefix)) {
            response = processPlayerStats(tournament, query);
        }
        else {
            throw new CalculatorException(CalculatorException.ErrorCode.QUERY_INVALID, String.format(CalculatorException.ErrorCode.QUERY_INVALID.description, query));
        }
        return response;
    }

    private String processScoreMatchQuery(Tournament tournament, String query) throws CalculatorException {
        String response = null;
        String matchName = query.substring(Constants.QueryRequest.SCORE_MATCH.prefix.length()).trim();

        Optional<Match> match = tournament.getMatches().stream().filter(m -> m.getName().equals(matchName)).findAny();
        if(match.isPresent()) {

            Player matchWinner = ScoreCalculator.calculateMatchWinner(match.get().getPlayers()[0], match.get().getPlayers()[1]);
            Player matchLooser = Arrays.stream(match.get().getPlayers()).filter(player -> !player.getName().equals(matchWinner.getName())).findFirst().get();

            if(matchWinner != null) {
                StringBuilder builder = new StringBuilder();
                builder.append(
                    String.format(
                        Constants.QueryResponse.MATCH_WINNER_MESSAGE.response,
                        matchWinner.getName(),
                        matchLooser.getName()));
                builder.append(System.lineSeparator());
                builder.append(String.format(Constants.QueryResponse.MATCH_WINNER_SETS_COMPARISION.response, matchWinner.getSetsWon(), matchLooser.getSetsWon()));

                response = builder.toString();
            }
            else {
                throw new CalculatorException(CalculatorException.ErrorCode.QUERY_NOT_ENOUGH_SETS_TO_DECIDE_WINNER, String.format(CalculatorException.ErrorCode.QUERY_NOT_ENOUGH_SETS_TO_DECIDE_WINNER.description, query));
            }
        }
        else {
            throw new CalculatorException(CalculatorException.ErrorCode.QUERY_MATCH_NAME_NOT_PRESENT, String.format(CalculatorException.ErrorCode.QUERY_MATCH_NAME_NOT_PRESENT.description, query));
        }
        return response;
    }

    private String processPlayerStats(Tournament tournament, String query) throws CalculatorException {

        String playerName = query.substring(Constants.QueryRequest.GAMES_PLAYER.prefix.length()).trim();

        AtomicBoolean playerExists = new AtomicBoolean(false);
        MutableInt gamesPlayedByPlayer = new MutableInt(0);
        MutableInt gamesWonByPlayer = new MutableInt(0);

        tournament.getMatches()
            .stream()
            .forEach(match -> {

                Optional<Player> player = Arrays.stream(match.getPlayers()).filter(p -> p.getName().equals(playerName)).findAny();
                if(player.isPresent()) {
                    playerExists.set(true);

                    Player opponent = Arrays.stream(match.getPlayers()).filter(p -> !p.getName().equals(playerName)).findAny().get();
                    gamesWonByPlayer.add(player.get().getGamesWon());
                    gamesPlayedByPlayer.add(opponent.getGamesWon());
                }
            });

        if(!playerExists.get()) {
            throw new CalculatorException(
                CalculatorException.ErrorCode.QUERY_MATCH_PLAYER_NOT_PRESENT,
                String.format(CalculatorException.ErrorCode.QUERY_MATCH_PLAYER_NOT_PRESENT.description, query));
        }

        return String.format(Constants.QueryResponse.GAMES_PLAYED.response, gamesWonByPlayer.toString(), gamesPlayedByPlayer.toString());
    }
}
