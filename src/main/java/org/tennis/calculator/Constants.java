package org.tennis.calculator;

public class Constants {

    /**
     * Enum to hold matches to process tournament data
     */
    public enum TournamentDataMatches {
        MATCH_DATA("Match:"),
        PLAYERS_SEPARATOR("vs");

        public final String matches;

        private TournamentDataMatches(String matches) {
            this.matches = matches;
        }
    }

    /**
     * Enum to hold all valid query requests that the program support
     */
    public enum QueryRequest {
        SCORE_MATCH("Score Match"),
        GAMES_PLAYER("Games Player");

        public final String prefix;

        private QueryRequest(String prefix) {
            this.prefix = prefix;
        }
    }

    public enum QueryResponse {
        MATCH_WINNER_MESSAGE("%s defeated %s"),
        MATCH_WINNER_SETS_COMPARISION("%s sets to %s"),
        GAMES_PLAYED("%s %s");

        public final String response;

        private QueryResponse(String response) {
            this.response = response;
        }
    }
}
