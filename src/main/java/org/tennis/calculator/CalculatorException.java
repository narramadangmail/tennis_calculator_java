package org.tennis.calculator;

public class CalculatorException extends Exception {

    public enum ErrorCode {
        // Program Input
        TOURNAMENT_FILE_NOT_PROVIDED("Complete path to Tournament File is required to proceed further with the program"),
        QUERY_PARAM_NOT_PROVIDED("No Query request provided. Please provide at least one query to proceed further with the program"),

        // Tournament file error codes
        TOURNAMENT_FILE_NOT_FOUND("Tournament File provided cannot be found - %s"),
        TOURNAMENT_FILE_EMPTY("Tournament File provided has no data - %s"),

        // Tournament File content is not valid
        INVALID_CONTENT_IN_TOURNAMENT_FILE("Tournament File is configured with invalid content - %s"),
        MATCH_CONTAINS_MORE_THAN_TWO_PLAYERS("Two Players should be tagged to a match - %s"),
        DUPLICATE_MATCH_NAMES("Duplicate match names found in tournament data"),
        DUPLICATE_PLAYERS_FOR_MATCH("Duplicate player names found for match - %s"),
        GAME_DATA_NOT_COMPLETE("Incomplete game data provided for match - %s"),

        // Query request validation error codes
        NO_QUERY_TO_PROCESS("No Query to process"),
        QUERY_INVALID("Invalid Query provided - %s"),
        QUERY_MATCH_NAME_NOT_PRESENT("Match name provided in query doesn't exist in tournament data - %s"),
        QUERY_MATCH_PLAYER_NOT_PRESENT("Player didn't play any match in this tournament - %s"),
        QUERY_NOT_ENOUGH_SETS_TO_DECIDE_WINNER("Players didn't play enough sets to pick a winner - %s");

        public final String description;
        private ErrorCode(String description) {
            this.description = description;
        }
    }

    private ErrorCode error;

    public CalculatorException(ErrorCode error, String message) {
        super(message);
        this.error = error;
    }

    public CalculatorException(ErrorCode error, String message, Exception e) {
        super(message, e);
        this.error = error;
    }

    public ErrorCode getError() {
        return error;
    }
}
