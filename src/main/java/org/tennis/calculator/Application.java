package org.tennis.calculator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.tennis.calculator.model.Tournament;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Application {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {

            // The first line should be tournament file path
            String tournamentFilePath = scanner.nextLine();
            if(StringUtils.isBlank(tournamentFilePath))
                throw new CalculatorException(
                    CalculatorException.ErrorCode.TOURNAMENT_FILE_NOT_PROVIDED,
                    CalculatorException.ErrorCode.TOURNAMENT_FILE_NOT_PROVIDED.description);

            // Loop through the next step of inputs to capture
            List<String> queryRequests = new ArrayList<>();
            String input;
            while(!(input = scanner.nextLine()).isEmpty() && !input.equalsIgnoreCase("EOF")) {
                queryRequests.add(input);
            }

            // Throw error if query not provided
            if(CollectionUtils.isEmpty(queryRequests))
                throw new CalculatorException(CalculatorException.ErrorCode.QUERY_PARAM_NOT_PROVIDED, CalculatorException.ErrorCode.QUERY_PARAM_NOT_PROVIDED.description);

            // Parse file and process tournament data
            TournamentDataProcessor calculator = new TournamentDataProcessor();

            List<String> tournamentData = calculator.parseTournamentFile(tournamentFilePath);
            Tournament tournament = calculator.processTournamentData(tournamentData);
            //System.out.println(tournament.toString());

            // Validate Tournament data for any data quality issues
            TournamentDataValidator.validateTournamentData(tournament);

            // Process queries
            QueryProcessor queryProcessor = new QueryProcessor();
            List<String> responses = queryProcessor.processRequestQueries(tournament,queryRequests);

            System.out.print(System.lineSeparator());
            System.out.println(responses.stream().collect(Collectors.joining(System.lineSeparator()+System.lineSeparator())));
        }
        catch (CalculatorException e) {
            System.out.println(e.getMessage());
        }
        finally {
            scanner.close();
        }
    }
}