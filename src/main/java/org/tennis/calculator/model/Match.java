package org.tennis.calculator.model;

public class Match {

    private String name;
    private Player[] players = new Player[2];

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(String player0, String player1) {
        players[0] = new Player(player0);
        players[1] = new Player(player1);
    }
}
