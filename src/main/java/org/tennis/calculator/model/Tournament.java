package org.tennis.calculator.model;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.List;

public class Tournament {

    private List<Match> matches = new ArrayList<>();

    public List<Match> getMatches() {
        return matches;
    }

    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return gson.toJson(this);
    }
}
