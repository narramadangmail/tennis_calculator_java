package org.tennis.calculator.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Player {

    private String name;
    private AtomicInteger gamePoint = new AtomicInteger();
    private AtomicInteger gamesWon = new AtomicInteger();
    private AtomicInteger setsWon = new AtomicInteger();

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getGamePoint() {
        return gamePoint.get();
    }

    public void markGamePoint() {
        gamePoint.incrementAndGet();
    }

    public void resetGamePoints() {
        gamePoint.set(0);
    }

    public int getGamesWon() {
        return gamesWon.get();
    }

    public void markGameWon() {
        if(gamesWon.incrementAndGet()%6 == 0) {
            markSetsWon();
        }
    }

    public int getSetsWon() {
        return setsWon.get();
    }

    public void markSetsWon() {
        setsWon.incrementAndGet();
    }
}
