package org.tennis.calculator;

/**
 * Utility wrapper class to hold exception thrown within lambda calls
 */
public class WrappedException extends RuntimeException {

    public WrappedException(Throwable e) {
        super(e);
    }
}
